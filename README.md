# Before Architecting

Architecting a software is a 3 step process. 
```mermaid
graph TD;
    Requirements[Understand Requirements];
    Architect[Design Architecture];
    Document[Document Architecture];
    Requirements --> Architect --> Document;
```

In this project we would focus upon Designing Architecture, and Documenting it. 
But architecting without expanding and understanding the requirements is a [very expensive mistake](https://www.construx.com/wp-content/uploads/resources/posters/Defect%20Cost%20Increase%20Poster%20by%20Construx.pdf). 
So please spend a few days (or hours if you are working on a 'tiny project') to Understand Requirements. 
It is important to ensure that you got 'requirements in full' - it is often the case that 
the team members who provide you requirements their documentation is 'work in progress' - 
you have to use those WIP requirements carefully to turn that into Architecture. You may have to 
take lead in **expanding the requirements** of your project, or involve others to get the requirements 
'fully correct' before starting with archicture here. More [about Requirements here](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements).

Once you have understood requirements fully, you can design the archirecture.

# Architecting
Inspired from [What is Architecture, from Clean Architecture book](https://learning.oreilly.com/library/view/clean-architecture-a/9780134494272/ch15.xhtml#ch15)<sup>[Get Paid Access](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)</sup>
we expect you to answer how your architecture aids:
* ~Development
* ~Deployment
* ~Operation
* ~Maintainence

Once answer to above 4 questions is provided (and is peer reviewed and approved by others), we assume you have provided answers to the following 2 questions as well
* ~"Architecture Pattern"
* ~"Communication Pattern"  

We were inspired for above 2 questions from [Software Architecture Map](https://github.com/AlaaAttya/software-architect-roadmap)

Down the line (not immediately, but in a few weeks because we are trying to [Keep the Options Open](https://learning.oreilly.com/library/view/clean-architecture-a/9780134494272/ch15.xhtml#ch15)) it is OK if your architecture matures and answers the questions on ~"Design Pattern"

And you are ready to document your finished architecture using C4model.com. There are several kinds of diagrams in there 
but we often design till the depth of Level1, Level2 and Level3 diagram. Sometimes deployment diagram as well. 
There is a tool https://structurizr.com/ we use often to document the architecture

# Contributing
Create issues and label them as `~"Design Pattern"`, `~"Architecture Pattern"`, `~"Communication Pattern"`, `~Development`, `~Deployment`, `~Operation`, `~Maintainence`. In those issues provide 
link to external articles which detail the patterns in detail. Discuss the patterns using issue comments

# Inspirations
* [Software Architect Roadmap](https://github.com/AlaaAttya/software-architect-roadmap)
* [Clean Architecture Book](https://learning.oreilly.com/library/view/clean-architecture-a/9780134494272/) <sup>[Get Paid Access](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)</sup>
* [Get Your Hands Dirty on Clean Architecture](https://learning.oreilly.com/library/view/get-your-hands/9781839211966/#toc) <sup>[Get Paid Access](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)</sup>

# Beyond Architecture
While you are reading this, your goal is to improve software architecture quality. Your higher level goal is to improve the software quality.
Software quality is driven by collaboration throughout [the entire development lifecycle](https://smartbear.com/blog/2016/improving-code-quality-and-collaboration/).
So while implementing the above practices on architecture, do check how you can [improve the entire development lifecycle at Smarter.Codes](https://gitlab.com/smarter-codes/guidelines/software-engineering/software-engineering-foundations)

